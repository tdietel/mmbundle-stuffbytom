#!/usr/local/bin/python3

import os
import re
import sys
import email
from datetime import datetime
import dateutil.parser

verbosity=6

logf = open("/Users/tom/.mailmate/sacern.log", "a")

subject = "bla"

# if verbosity>5:
logf.write("reading email msg from stdin\n")

#sys.stdin = sys.stdin.detach()
msg = email.message_from_file(sys.stdin)


counter = 1
for part in msg.walk():

    # multipart/* are just containers
    if part.get_content_maintype() == 'multipart':
        continue

    # Applications should really sanitize the given filename so that an
    # email message can't be used to overwrite important files
    filename = part.get_filename()
    content_type = part.get_content_type()

    if verbosity>5:
        logf.write( "processing part %d: filename=%s type=%s\n"
                    % (counter,filename, content_type))

    # ------------------------------------------------------------------
    # Some manual fiddling with recurrent malformatted emails
    if filename:
        m = re.search("T274395-([A-Z]+)/([0-9]+).PDF",filename)
        if m:
            outfile = "T274395-%s-%s.PDF" % m.group(1,2)
            content_type = "application/pdf"
        else:
            outfile = filename

    else: # no filename given
        if content_type == 'text/html':
            outfile = "BODY.html"
        elif content_type == 'text/plain':
            outfile = "BODY.txt"
        else:
            continue


    logf.write( "exporting part %d (%s) as %s\n"
                % (counter-1, content_type, outfile))
    with open(outfile, 'wb') as f:
        f.write(part.get_payload(decode=True))
