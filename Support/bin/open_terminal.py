#!/usr/bin/env python3
import iterm2
import sys

# In principle, this should be a more powerful alternative to AppleScript.
# In practice, I did not manage to make the Python API raise the window when
# running MailMate as the main application.

command = f"""
  echo "Hello,there" ;\
  date ;\
  cd {sys.argv[1]} ;\
  exec /bin/bash -i
"""

# open a terminal in the created folder
async def term(connection):
    window = await iterm2.Window.async_create(connection, command=f"sh -c '{command}'")
    bla = await window.async_activate()
    bla = await window.async_select()

iterm2.run_until_complete(term)
